import React, {Component} from 'react';
import { View, Text, ScrollView, FlatList, StyleSheet, Modal, Button, Alert, PanResponder, Share} from 'react-native';
import {Card, Icon, Input} from 'react-native-elements';
import {connect} from 'react-redux'
import {baseUrl} from '../shared/baseUrl'
import * as Animatable from 'react-native-animatable'
import {Rating} from 'react-native-ratings'

import {postFavourite, postComment} from '../redux/ActionCreators'


const mapStateToProps = state => {
    return{
        dishes: state.dishes,
        comments: state.comments,
        favourites: state.favourites
    }
}

const mapDispatchToProps = dispatch => ({
    postFavourite: (dishId) => dispatch(postFavourite(dishId)),
    postComment: (dishId,author, rating, comment) => dispatch(postComment(dishId,author,rating,comment))
});

function RenderDish(props) {
    handleViewRef = ref => this.view = ref;


    const recognizeDrag = ({moveX, moveY, dx, dy}) => {
        if (dx < -100)
            return 1;
        else if (dx > 100)
            return 2;
        else
            return 0;
    };

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
            .then(endState => console.log((endState.finished ? 'finished' : 'cncelled')))
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState) === 1){
                Alert.alert(
                    'Add to Favourites?',
                    'Are you sure you wish to add ' + dish.name + ' to favourites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('cancelled'),
                            style: 'cancel'
                        },
                        {
                            text: 'OK',
                            onPress: () => {props.favourite ? console.log('already favourite') : props.onPress()}
                        }
                    ],
                    {cancelable: false}
                )}
            else if (recognizeDrag(gestureState) === 2){
                {props.onPressModal()}
            }
            return true;
        }
    });
    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ':' + message + ' ' + url,
            url: url
        },{
            dialogTitle: 'Share' + title
        })
    }
    const dish=props.dish;
    if (dish!= null){
        return(
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                ref={this.handleViewRef}
                {...panResponder.panHandlers}>
                <Card
                    featuredTitle={dish.name}
                    image={{uri: baseUrl + dish.image}}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <View style={styles.row}>
                        <Icon 
                            raised
                            reverse
                            name={props.favourite ? 'heart' : 'heart-o'}
                            type='font-awesome'
                            color='#f50'
                            onPress={() => props.favourite ? console.log('already favourite') : props.onPress()}
                            />
                        <Icon 
                            raised
                            reverse
                            name='pencil'
                            type='font-awesome'
                            color="#512DA8"
                            onPress={() => props.onPressModal()}
                            />
                        <Icon
                            raised
                            reverse
                            name='share'
                            type='font-awesome'
                            color="#51D2A8"
                            onPress={() => shareDish(dish.name,dish.description,baseUrl + dish.image)}
                            />
                        </View>
                </Card>
            </Animatable.View>
        );

    }
    else {
        return(
            <View></View>
        );
    }
}

function RenderComments(props){
    const comments = props.comments;
    const renderCommentItem = ({ item, index}) => {
        return(
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>
                    {item.comment}
                </Text>
                <Rating readonly style={styles.rating} imageSize={12} ratingCount={item.rating}/>
                <Text style={{fontSize: 12}}>{'-- '+item.author + ', '+item.date}</Text>
            </View>
        )
    }

    return(
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card title="Comments">
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

class Dishdetail extends Component  {
    constructor(props){
        super(props);
        this.state={
            favourites:[],
            showModal: false,
            rating:4,
            author:'',
            comment:''
        }
    }

    resetForm(){
        this.setState({
            favourites:[],
            showModal: false,
            rating:4,
            author:'',
            comment:''
        })
    }

    submitForm(){
        this.props.postComment(this.props.route.params.dishId,this.state.author,this.state.rating,this.state.comment);
        this.toggleModal();
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal})
    }

    markFavourite(dishId){
        this.props.postFavourite(dishId)
    }

    render(){
        const dishId = this.props.route.params.dishId;
        return(
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[+dishId]} 
                    favourite={this.props.favourites.some(el => el === dishId)}
                    onPress={() => this.markFavourite(dishId)}
                    onPressModal={() => this.toggleModal()}
                    /> 
                <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)}/>
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.showModal}
                    onDismiss={() => {this.toggleModal()}}
                    onRequestClose = {() => {this.toggleModal()}}
                >
                    <View style={styles.col}>
                        <View style={styles.row}>
                            <Rating
                                    ratingCount={5}
                                    startingValue={4}
                                    imageSize={40}
                                    showRating
                                    onFinishRating={(value => this.setState({rating:value}))}
                            />
                        </View>
                        <View style={styles.col}>
                            <View style={styles.row}>
                                <Input
                                    placeholder=' Author'
                                    leftIcon={
                                        <Icon
                                            name='user'
                                            size={24}
                                            type='font-awesome'
                                            color='black'
                                        />
                                    }
                                    onChangeText={(value => this.setState({author:value}))}
                                />
                            </View>
                            <View style={styles.row}>
                                <Input
                                    placeholder=' Comment'
                                    leftIcon={
                                        <Icon style={styles.horMar}
                                            name='user'
                                            type='font-awesome'
                                            size={24}
                                            color='black'
                                        />
                                    }
                                    onChangeText={(value => this.setState({comment:value}))}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                        <Button
                            raised
                            title="Submit Comment"
                            color="#512DA8"
                            onPress={() => this.submitForm()}
                        />
                        </View>
                        <View style={styles.row}>
                        <Button
                            raised
                            title="Cancel"
                            onPress={() => {this.resetForm()}}
                        />
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'row',
        margin:10
    },
    col:{
        margin:20,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'column'
    },
    mar:{
        flexDirection:'column',
        margin:2
    },
    rating:{
        flexDirection:'row',
        justifyContent:'flex-start'
    }
    }
)

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail)