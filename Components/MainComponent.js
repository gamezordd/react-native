import React, {Component} from 'react';
import {DISHES} from '../shared/dishes'
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer';
import {Icon} from 'react-native-elements';
import {Image, StyleSheet, SafeAreaView,View, Text, ToastAndroid} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import {fetchComments, fetchDishes, fetchLeaders, fetchPromos} from '../redux/ActionCreators'
import {connect} from 'react-redux'
import NetInfo from '@react-native-community/netinfo';


import Login from './LoginComponent'
import Menu from './MenuComponent.js';
import Home from './HomeComponent'
import AboutComponent from './AboutComponent'
import Dishdetail from './DishDetailComponent.js';
import ContactComponent from './ContactComponent'
import Reservation from './ReservationComponent'
import Favourites from './FavouriteCOmponent'

const LoginNavigator = createStackNavigator();
const FavouritesNavigator = createStackNavigator();
const ReservationNavigator = createStackNavigator();
const AboutNavigator = createStackNavigator();
const ContactNavigator = createStackNavigator();
const MenuNavigator = createStackNavigator();
const HomeNavigator = createStackNavigator();
const MainNavigator = createDrawerNavigator();


const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        promotions: state.promotions,
        leaders: state.leaders
    }
}

const mapDispatchToProps = dispatch => ({
    fetchComments: () => dispatch(fetchComments()),
    postComment: (comment) => dispatch(postComment(comment)),
    fetchDishes: () => dispatch(fetchDishes()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders: () => dispatch(fetchLeaders())

})

const CustomDrawerContentComponent = (props) => (
    <ScrollView>
        <SafeAreaView style={styles.container}
            forceInset={{top: 'always', horizontal:'never'}}>
        <View style={styles.drawerHeader}>
            <View style={{flex: 1}}>
                <Image source={require('./images/logo.png')}
                    style={styles.drawerImage} />
            </View>
            <View style={{flex: 2}}>
                <Text style={styles.drawerHeaderText}>
                    Ristorante Con Fusion
                </Text>
            </View>
        </View>
        <DrawerItemList {...props}/>
        </SafeAreaView>
    </ScrollView>
);

function LoginNavigatorScreen(){
    return(
        <LoginNavigator.Navigator
            initialRouteName="Login"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}
            >
            <LoginNavigator.Screen
                name= "Login"
                component={Login}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
            />
        </LoginNavigator.Navigator>
    )
}

function FavouritesNavigatorScreen() {
    return(
        <FavouritesNavigator.Navigator
            initialRouteName="Favourites"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}
            >
            <FavouritesNavigator.Screen
                name="Favourites"
                component={Favourites}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
                />
            <FavouritesNavigator.Screen
                name="Dishdetail"
                component={Dishdetail}
                options={{headerTitle: "Dish Detail"}}   
            />
        </FavouritesNavigator.Navigator>
    )
}

function ReservationNavigatorScreen() {
    return(
        <ReservationNavigator.Navigator
            initialRouteName="Reserve Table"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}>
            <ReservationNavigator.Screen 
                name = "Reserve Table"
                component={Reservation}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
                >
            </ReservationNavigator.Screen>
        </ReservationNavigator.Navigator>
    );
}

function ContactNavigatorScreen() {
    return(
        <ContactNavigator.Navigator
            initialRouteName="Contact Us"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}>
            <ContactNavigator.Screen name = "Contact Us"
                component={ContactComponent}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
            />
        </ContactNavigator.Navigator>
    );
}

function AboutNavigatorScreen() {
    return(
        <AboutNavigator.Navigator
            initialRouteName= "About Us"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}>
            <AboutNavigator.Screen name="About Us"
                component={AboutComponent}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
            />
        </AboutNavigator.Navigator>
        
    );
}

function HomeNavigatorScreen(){
    return(
        <HomeNavigator.Navigator 
            initialRouteName = "Home"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}>
            <HomeNavigator.Screen name="Home" component={Home}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}/>
        </HomeNavigator.Navigator>
    );
}

function MenuNavigatorScreen() {
    return(
        <MenuNavigator.Navigator
            initialRouteName="Menu"
            screenOptions={{
                headerStyle: {
                    backgroundColor:"#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: '#fff'
                }
            }}
        >
            <MenuNavigator.Screen
                name="Menu"
                component={Menu}
                options={({navigation}) => ({
                    headerLeft: () => (
                        <Icon name="menu" size={24} color="white"
                            onPress={() => navigation.toggleDrawer()}/>
                    )
                })}
            />
            <MenuNavigator.Screen
                name="Dishdetail"
                component={Dishdetail}
                options={{headerTitle: "Dish Detail"}}   
                />
        </MenuNavigator.Navigator> 
    )
}

function MainNavigatorScreen(){
    return(
        <MainNavigator.Navigator drawerContent={(props) => <CustomDrawerContentComponent {...props}/>}>
            <MainNavigator.Screen
                name= "Home"
                component={HomeNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='home' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
            <MainNavigator.Screen
                name="Favourites"
                component={FavouritesNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='list' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}/>
            <MainNavigator.Screen
                name="Menu"
                component={MenuNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='list' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
            <MainNavigator.Screen
                name="Reserve Table"
                
                component={ReservationNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='cutlery' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
            <MainNavigator.Screen
                name="About Us"
                component={AboutNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='info-circle' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
            <MainNavigator.Screen
                name="Contact Us"
                component={ContactNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='address-card' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
            <MainNavigator.Screen
                name="Login"
                component={LoginNavigatorScreen}
                options={({tintColor}) => ({
                    drawerIcon: () => (
                        <Icon name='sign-in' type='font-awesome' size={24} color={tintColor}/>
                    )
                })}
            />
        </MainNavigator.Navigator>
    );
}

class Main extends Component {

    componentDidMount() {
        this.props.fetchComments();
        this.props.fetchDishes();
        this.props.fetchLeaders();
        this.props.fetchPromos();

        NetInfo.fetch().then((connectionInfo) => {
            ToastAndroid.show('Initial Network Connectivity Type: '
                + connectionInfo.type + ', effectiveType: ',ToastAndroid.LONG)
        });
        NetInfo.addEventListener(connectionChange => this.handleConnectivityChange(connectionChange))
    }
    componentWillUnmount() {
        NetInfo.removeEventListener(connectionChange => this.handleConnectivityChange(connectionChange))
    }

    handleConnectivityChange = (connectionInfo) => {
        switch (connectionInfo.type) {
            case 'none': 
                ToastAndroid.show ('You are now offline', ToastAndroid.LONG);
                break;
            case 'wifi':
                ToastAndroid.show ('You are now on WiFi', ToastAndroid.LONG);
                break;
            case 'cellular':
                ToastAndroid.show ('You are now on Cellular', ToastAndroid.LONG);
                break;
            case 'unknown' :
                ToastAndroid.show ('You are now have an Unknown connection', ToastAndroid.LONG);
                break;
            default: 
        }
    }

    constructor (props){
        super(props);
        this.state = {
            dishes: DISHES,
            selectedDish: null
        }
    }

    onDishSelect(dishId){
        this.setState({selectedDish: dishId});
    }

    render(){
        return(
            <NavigationContainer>
                <MainNavigatorScreen/>
            </NavigationContainer>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    drawerHeader: {
        backgroundColor: '#512DA8',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin:10,
        width: 80,
        height: 60
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);