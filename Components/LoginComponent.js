import React, {Component} from 'react';
import { View, Button, StyleSheet, Text, ScrollView, Image, Alert} from 'react-native';
import {Icon, Input, CheckBox} from 'react-native-elements'
import * as SecureStore from'expo-secure-store'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import {baseUrl} from '../shared/baseUrl'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as ImageManipulator from 'expo-image-manipulator'

class RegisterTab extends Component{
    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            email:'',
            remember: false,
            imageUrl: baseUrl + 'images/logo.png'
        }
    }

    getImageFromGallery = async () => {
        var { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if (status !== 'granted'){
            while(status !=='granted'){
                Alert.alert('Please allow permissions to import image')
                status = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            }
        }

        try{
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4,3],
                quality:0.50
            });
            if (!result.cancelled){
                this.processImage(result.uri)
            }
            
        }
        catch(E){
            Alert.alert('Error: ', E)
        }
    }

    getImageFromCamera = async () => {
        const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRollPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if(cameraPermission.status === 'granted' && cameraRollPermission.status === 'granted'){
            let capturedImage = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4,3]
            });

            if (!capturedImage.cancelled){
                this.processImage(capturedImage.uri);
            }
        }
    }

    processImage = async (imageUri) => {
        let processedImage = await ImageManipulator.manipulateAsync(
            imageUri,
            [
                {resize: {width: 400}}
            ],
            {format: 'png'}
        );
        this.setState({imageUrl: processedImage.uri})
    }

    handleRegister(){
        console.log(JSON.stringify(this.state));
        if (this.state.remember){
            SecureStore.setItemAsync(
                'userInfo',
                JSON.stringify({username: this.state.username, password: this.state.password})
            )
            .catch((error) => console.log('Could not save info: ', error))
        }
    }

    render(){
        return (
            <ScrollView>
                <View style={styles.imageContainer}>
                    <Image 
                        source={{uri: this.state.imageUrl}}
                        loadingIndicatorSource= {require('./images/logo.png')}
                        style={styles.image}
                    />
                    <View style={styles.btncontainer}>
                        <View style={{flex:1, paddingHorizontal:10}}>
                            <Button 
                                title='Camera'
                                
                                onPress={this.getImageFromCamera}
                                />
                        </View>
                        <View style={{flex:1, paddingHorizontal:10}}>
                            <Button
                                title='Gallery'
                                onPress={this.getImageFromGallery}
                                />
                        </View>
                    </View>
                </View>
                <View style={styles.container}>
                    <Input
                        placeholder="Username"
                        leftIcon={{type: 'font-awesome', name: 'user-o'}}
                        onChangeText={(username) => this.setState({username})}
                        value={this.state.username}
                        inputContainerStyle={styles.formInput}
                    />
                    <Input
                        placeholder="Password"
                        leftIcon={{type: 'font-awesome', name: 'key'}}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        inputContainerStyle={styles.formInput}
                    />
                    <Input
                        placeholder="Firstname"
                        leftIcon={{type: 'font-awesome', name: 'user-o'}}
                        onChangeText={(firstname) => this.setState({firstname})}
                        value={this.state.firstname}
                        inputContainerStyle={styles.formInput}
                    />
                    <Input
                        placeholder="Last Name"
                        leftIcon={{type: 'font-awesome', name: 'user-o'}}
                        onChangeText={(lastname) => this.setState({lastname})}
                        value={this.state.lastname}
                        inputContainerStyle={styles.formInput}
                    />
                    <Input
                        placeholder="Email"
                        leftIcon={{type: 'font-awesome', name: 'envelope-o'}}
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        inputContainerStyle={styles.formInput}
                    />
                    <CheckBox
                        title="Remember Me"
                        center
                        checked={this.state.remember}
                        containerStyle={{backgroundColor:'rgba(52, 52, 52, 0)',borderColor:'rgba(52, 52, 52, 0)'}}
                        onPress={() => this.setState({remember: !this.state.remember})}
                    />
                    <View style={styles.formButton}>
                        <Button
                            onPress={() => this.handleRegister()}
                            title='Register' 
                            icon={<Icon name='user-plus' 
                                    size={24}
                                    type='font-awesome' 
                                    color='white'
                                    />
                                    }
                            buttonStyle={{backgroundColor:'#512DA8'}}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

class LoginTab extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            remember: false
        }
    }   

    componentDidMount() {
        SecureStore.getItemAsync('userInfo')
            .then((userdata) => {
                let userInfo = JSON.parse(userdata);
                if (userInfo){
                    this.setState({username: userInfo.username,
                        password: userInfo.password,
                        remember: true})
                }
            })
    }
    
    handleLogin() {
        console.log(JSON.stringify(this.state));
        if (this.state.remember){
            SecureStore.setItemAsync(
                'userInfo',
                JSON.stringify({username: this.state.username, password: this.state.password})
            )
            .catch((error) => console.log('Could not save info: ', error))
        }
        else {
            SecureStore.deleteItemAsync('userInfo')
            .catch((error) => console.log('could not delete info: ', error)
            )
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Input
                    placeholder="Username"
                    leftIcon={{type: 'font-awesome', name: 'user-o'}}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    inputContainerStyle={styles.formInput}
                />
                <Input
                    placeholder="Password"
                    leftIcon={{type: 'font-awesome', name: 'key'}}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    inputContainerStyle={styles.formInput}
                />
                    <CheckBox
                        title="Remember Me"
                        center
                        containerStyle={{backgroundColor:'rgba(52, 52, 52, 0)',borderColor:'rgba(52, 52, 52, 0)'}}
                        checked={this.state.remember}
                        onPress={() => this.setState({remember: !this.state.remember})}
                    />
                <View style={styles.formButton}>
                <Button
                    onPress={() => this.handleRegister()}
                    title='Login' 
                    icon={<Icon name='sign-in' 
                            size={24}
                            type='font-awesome' 
                            color='white'
                            />
                            }
                    buttonStyle={{backgroundColor:'#512DA8'}}
                />
                </View>
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.props.navigation.navigate('Register')}
                        title='Register' 
                        clear
                        icon={<Icon name='user-plus' 
                                size={24}
                                type='font-awesome' 
                                color='blue'
                                />
                                }
                        titleStyle={{color:'blue'}}
                    />
                </View>
            </View>
        );
    }
}


const tabNavigator = createBottomTabNavigator();

function Login() {
    return(
        <NavigationContainer 
            independent={true}
            opt
            >
            <tabNavigator.Navigator
                initialRouteName='Login'
                tabBarOptions={{
                    activeBackgroundColor: '#9575CD',
                    inactiveBackgroundColor: '#D1C4E9',
                    activeTintColor: '#ffffff',
                    inactiveTintColor: 'gray'
                }}>
                <tabNavigator.Screen 
                name='Login' 
                component={LoginTab}
                options={{
                    title: 'Login',
                    tabBarIcon:({ tintColor }) => (
                        <Icon
                          name='sign-in'
                          type='font-awesome'            
                          size={24}
                          iconStyle={{ color: tintColor }}
                        />
                      )
                }}
                />
                <tabNavigator.Screen 
                    name='Register' 
                    component={RegisterTab}
                    options={{
                        title: 'Register',
                        tabBarIcon:({ tintColor }) => (
                            <Icon
                              name='user-plus'
                              type='font-awesome'            
                              size={24}
                              iconStyle={{ color: tintColor }}
                            />
                          )
                    }}
                />
            </tabNavigator.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20
    },
    imageContainer: {
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    image: {
        margin: 10,
        width: 80,
        height: 60
    },
    formInput:{
        margin:20
    },
    formCheckbox:{
        margin:20,
        backgroundColor: 'black',
        
    },
    formButton: {
        margin:60
    },
    btncontainer:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1

    }
});


export default Login;