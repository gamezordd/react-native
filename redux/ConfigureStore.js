import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import {persistStore, persistCombineReducers} from 'redux-persist'
import {AsyncStorage} from 'react-native'

import {dishes} from './dishes'
import {comments} from './comments'
import {promotions} from './promotons'
import {leaders} from './leaders';
import {favourites} from './favourites'


export const ConfigureStore = () => {

    const config = {
        key: 'root',
        storage: AsyncStorage,
        debug: true
    }

    const store = createStore(
        persistCombineReducers(config, {
            dishes,
            comments,
            promotions,
            leaders,
            favourites
        }),
        applyMiddleware(thunk)
    );

    const persistor = persistStore(store);
    return {persistor, store};
}