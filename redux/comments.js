import * as ActionTypes from './ActionTypes'

export const comments = (state = {
    isLoading: true,
    errMess: null,
    comments: []
    }, action) => {
        switch(action.type) {
            case ActionTypes.ADD_COMMENTS:
                return {...state, isLoading: false, errMess: null, comments: action.payload};
            case ActionTypes.ADD_COMMENT:{
                
                {console.log('payload is: ', action.payload)}
                return {...state, comments: state.comments.concat(action.payload)};
            }
            default:
                return state
        }
    }